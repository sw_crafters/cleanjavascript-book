# Tipos de tests de software

Aunque en esta sección nos vamos a centrar en los test que escriben los desarrolladores, en concreto en los test unitarios, creo que es interesante realizar, a grandes rasgos, una clasificación de los diferentes tipos que existen. Para ello lo primero es contestar a la pregunta:

## ¿Qué entendemos por testing?

El *testing* de *software* se define como un conjunto de técnicas que se utilizan para verificar que el sistema desarrollado, ya sea por nosotros o por terceros, cumple con los requerimientos establecidos.

## Test manuales vs automáticos

La primera gran diferenciación que podemos realizar es atendiendo a cómo se realiza su ejecución, es decir, a si se hace de forma manual o automática. El *testing* manual consiste en preparar una serie de casos y ejecutar a mano los elementos necesarios, como podrás imaginar tiene muchísimas limitaciones, ya que son lentos, difíciles de replicar, caros y además cubren muy pocos casos. Es por ello que la mayoría de los test deberían de ser automáticos.

Aunque debemos ser conscientes de que no todo es automatizable, ya que a veces se dan casuísticas en las que se vuelve muy costoso, o directamente imposible, automatizar ciertas situaciones y no queda más remedio que hacer ciertas pruebas de forma manual. De ahí la importancia de contar con un buen equipo de QA que complemente al equipo de desarrollo.

## Test funcionales vs no funcionales

Una forma quizás más intuitiva de clasificar los [diferentes tipos de test de software que existen](https://www.softwaretestinghelp.com/types-of-software-testing/) es agrupándolos en test funcionales y test no funcionales.

### Tests funcionales
Los test funcionales hacen referencia a las pruebas que verifican el correcto comportamiento del sistema, subsistema o componente *software*. Es decir, validan que el código cumpla con las especificaciones que llegan desde negocio y que además esté libre de bugs. Dentro de este tipo de pruebas encontramos principalmente las siguientes:

* **Tests Unitarios:** Este tipo de pruebas comprueban elementos básicos de nuestro *software* de forma aislada. Son los test más importantes a la hora de validar las reglas de negocio que hemos desarrollado. Nos centraremos en este tipo de pruebas a lo largo de la sección de *testing*.

* **Tests de integración:** Los test de integración son aquellos que prueban conjuntos de elementos básicos, normalmente suelen incluirse en este tipo de pruebas algunos elementos de infraestructura, como base de datos o llamadas a APIs.

* **Tests de sistema:** Este tipo de test, también denominados end-to-end o de extremo a extremo, prueban múltiples elementos de nuestra arquitectura simulando el comportamiento de un actor con nuestro software.

* **Tests de regresión:** Este tipo de pruebas se encargan de verificar la funcionalidad ya entregada, es decir, son pruebas que se usan para detectar que en los cambios introducidos en el sistema no se genera un comportamiento inesperado. En definitiva, cualquier tipo de test funcional de los que hemos visto podría ser un test de regresión, siempre y cuando hayan pasado correctamente en algún momento y, tras realizar algún cambio en el sistema, empiecen a fallar.

Además de este tipo de test funcionales puedes encontrar algunos más con nomenclatura diferente como podrían ser: *sanity testing*, *smoke testing*, *UI testing*, *Beta/Acceptance testing*, etc. Todos ellos pertenecen a uno o a varios de los tipos de test funcionales anteriores.

### Tests no funcionales

El objetivo de los test no funcionales es la verificación de un requisito que especifica criterios que pueden usarse para juzgar la operación de un sistema, como por ejemplo la disponibilidad, accesibilidad, usabilidad, mantenibilidad, seguridad y/o rendimiento. Es decir, a diferencia de los funcionales, se centran en comprobar cómo responde el sistema, no en qué hace o debería hacer.

Podemos clasificar los test no funcionales según el tipo de requisito no funcional que abarcan, entre todos ellos destacan:

* **Tests de carga:** Son test mediante los que se observa el comportamiento de una sistema *software* bajo diferentes números de peticiones durante un tiempo determinado.

*  **Tests de velocidad:** Comprueban si el sistema genera los resultados en un tiempo aceptable.

* **Tests de usabilidad:** Son pruebas en las que se trata de evaluar la UX del sistema. 

*  **Tests de seguridad:**  Se trata de un conjunto de pruebas en las que se trata de evaluar si el sistema desarrollado está expuesto a vulnerabilidades conocidas. 

Tanto los test no funcionales como los funcionales siguen el mismo proceso a la hora de generarlos:

* **Escenario inicial**: Se crean una serie de datos de entrada para poder ejecutar las pruebas.

* **Ejecución del tests**: Se ejecuta la prueba con sus correspondientes datos de entrada sobre el sistema.

* **Evaluación del resultado**: El resultado obtenido se analiza para ver si coincide con lo esperado.

Normalmente los test no funcionales son generados por el equipo de QA, mientras que los test funcionales suelen ser creados por los desarrolladores, sobre todo los test unitarios, de integración y la mayoría de los test de sistema.

## Pirámide de testing

La Pirámide de Testing, también conocida como Pirámide de Cohn (por su autor [Mike Cohn](https://en.wikipedia.org/wiki/Mike_Cohn)), es una forma muy extendida y aceptada de organizar los test funcionales en distintos niveles, siguiendo una estructura con forma de pirámide:

![Pirámide de Testing.](images/testing_tipos_pyramid.png)

La pirámide es muy simple de entender, la idea es tratar de organizar la cantidad de test que tenemos en base a su velocidad de ejecución y al coste de crearlos y mantenerlos. Es por ello que los test unitarios aparecen en la base de la pirámide, ya que, si los diseñamos centrándonos en una sola unidad de *software* de forma aislada, son muy rápidos de ejecutar, fáciles de escribir y baratos de mantener.

En el otro extremo se encuentran los test *end-to-end* o de sistema. Como hemos mencionado. En estos test se prueba nuestro sistema de punta a punta, debido a esto entran en juego todos los elementos del sistema implicados en una acción concreta, con lo cual estos test se antojan lentos a la hora de ejecutar y complicados de crear y mantener. Generalmente buscamos tener pocos de estos test debido a su fragilidad y alto costo de mantenimiento.

La parte media de la pirámide está constituida por los test de integración, el objetivo de este tipo de pruebas es comprobar si las diferentes unidades de software interactúan con ciertos elementos de infraestructura como APIs externas o base de datos de la forma prevista. Este tipo de test son más lentos de ejecutar y complejos de escribir y mantener que los test unitarios, aunque en muchos contextos también aportan mayor seguridad. Por otro lado, este tipo de test son bastante más baratos y rápidos que los de sistemas, por esta razón lo ideal es que tengamos una cantidad intermedia de estos.

### Antipatrón del cono de helado

La Pirámide del Testing hay que entenderla como una recomendación y no tiene por qué encajar siempre con nuestro contexto, sobre todo en lo referente a la cantidad de test unitarios y de integración. Por ejemplo, podría darse el caso que el sistema software que estamos diseñando tenga pocas reglas de negocio, las cuales pueden ser cubiertas por unos cuantos test unitarios. En cambio, dicho sistema podría ser muy demandante a nivel de elementos externos, en ese caso es probable que nos interese tener más test de integración que unitarios.

Normalmente, contextos como el anterior suelen ser la excepción y, aun hoy en día, en muchos proyectos nos encontramos con el temido “antipatrón del cono de helado”:

![Pirámide de Testing invertida.](images/testing_tipos_icecream.png)

Como puedes observar, es la pirámide de *testing* invertida. En este contexto se centra el foco en muchas pruebas manuales y *end-to-end*. Esto acarrea múltiples problemas, el principal es que el coste de probar el sistema se dispara, ya que con pocos test de integración y unitarios (y muchas veces ninguno) se vuelve tremendamente complejo determinar dónde están los problemas cuando los test de nivel superior fallan.

<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE3MTI1MzA0NDMsLTE4ODQ3NjgyODYsMT
A5ODY3NDI1MCwtMzE2MjA3OTkwLDE0OTA0MTYzODUsLTE2MTgx
NTE3NDEsLTMwMzEyOTA4NSwtNzAxMTk4MTU4LDQ2OTg5MDUxNC
wtMTA5MjI5MTI2OSwtOTUzNTgxNDA0LC0zMTY1NjE0NjEsLTE1
MjI2NjU5MDEsMTA2Mzg5NjE4MiwtMTgwMzI5NjA5LDEzMzIzMj
Y4ODAsLTE4ODU4OTM3NDEsMjAyMjM0Mjk0LDE5MDY2ODk2Nzks
MzM2MDM0MjUwXX0=
-->