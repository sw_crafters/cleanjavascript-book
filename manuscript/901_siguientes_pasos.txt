# Siguientes pasos

En este *e-book* me he centrado en hacer hincapié en algunas buenas prácticas que hacen que nuestro código JavaScript sea legible, mantenible y testeable y, en definitiva, más tolerante al cambio. En futuras publicaciones profundizaremos en algunos conceptos por los que hemos pasado de puntillas, como por ejemplo el de arquitectura hexagonal. Probablemente trataremos este tema a través de ejemplos prácticos con NodeJS y TypeScript.

Por otro lado, una lectura que en mi opinión es muy recomendable y que además podemos entender como una continuación muy natural de los conceptos expuestos en la sección de “Testing”, es la nueva versión del libro de Carlos Blé, [Diseño ágil con TDD](https://leanpub.com/tdd-en-castellano). He tenido el placer de leer los primeros capítulos antes de que Carlos lo publicara y he de decir que está haciendo un gran trabajo. Tal y como él mismo dice, se trata de una edición totalmente nueva, actualizada, más práctica y fácil de leer.

Si te ha gustado el *e-book*, valóralo y compártelo en tus redes sociales. No dudes en plantear preguntas, aportes o sugerencias. ¡Estaré encantado de responder!
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTY0Nzc5NDEwMCwxNDM4NDYxMjY0LDg0MD
Q1NjkwMF19
-->