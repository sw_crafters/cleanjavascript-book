# SRP - Principio de responsabilidad única

**"Nunca debería haber más de un motivo por el cual cambiar una clase o un módulo".** -- Robert C. Martin

El primero de los cinco principios, *single responsibility principle* (SRP), principio de responsabilidad única en castellano, viene a decir que una clase debe tener tan solo una única responsabilidad. A finales de los 80, Kent Beck y Ward Cunningham ya aplicaban este principio mediante tarjetas CRC (Class, Responsibility, Collaboration), con las que detectaban responsabilidades y colaboraciones entre módulos.

Tener más de una responsabilidad en nuestras clases o módulos hace que nuestro código sea difícil de leer, de testear y mantener. Es decir, hace que el código sea menos flexible, más rígido y, en definitiva, menos tolerante al cambio.

La mayoría de veces, los programadores aplicamos mal este principio, ya que solemos confundir “tener una única responsabilidad” con “hacer una única cosa”. Es más, ya vimos un principio como este último en el capítulo de las funciones:  las funciones deben hacer una única cosa y hacerla bien. Este principio lo usamos para refactorizar funciones de gran tamaño en otras más pequeñas, pero esto no aplica a la hora de diseñar clases o componentes. 

## ¿Qué entendemos por responsabilidad?

El principio de responsabilidad única no se basa en crear clases con un solo método, sino en diseñar componentes que solo estén expuestos a una fuente de cambio. Por lo tanto, el concepto de responsabilidad hace referencia a aquellos actores (fuentes de cambio) que podrían reclamar diferentes modificaciones en un determinado módulo dependiendo de su rol en el negocio. Veamos un ejemplo:

{title="Principio de responsabilidad única", lang=javascript}
~~~~~~~
class UseCase{
  doSomethingWithTaxes(){
    console.log("Do something related with taxes ...")
  }

  saveChangesInDatabase(){
    console.log("Saving in database ...")
  }

  sendEmail(){
    console.log("Sending email ...")
  }
}

function start(){
  const myUseCase = new UseCase()

  myUseCase.doSomethingWithTaxes();
  myUseCase.saveInDatabase();
  myUseCase.sendEmail();
}

start();
~~~~~~~

Puedes acceder al ejemplo interactivo [desde aquí](https://repl.it/@SoftwareCrafter/SOLID-SRP).

En este ejemplo tenemos una clase *UseCase* que consta de tres métodos: *doSomethingWithTaxes()*, *sendEmail()* y *saveChangesInDatabase()*. A primera vista se puede detectar que estamos mezclando tres capas de la arquitectura muy diferenciadas: la lógica de negocio, la lógica de presentación y la lógica de persistencia. Pero, además, esta clase no cumple con el principio de responsabilidad única porque el funcionamiento de cada uno de los métodos son susceptibles a ser cambiados por tres actores diferentes. 

El método *doSomethingWithTaxes()* podría ser especificado por el departamento de contabilidad, mientras que *sendEmail()* podría ser susceptible a cambio por el departamento de *marketing* y, para finalizar, el método *saveChangesInDatabase()* podría ser especificado por el departamento encargado de la base de datos. 

Es probable que no existan, de momento, estos departamentos en tu empresa, y que la persona encargada de asumir estos puestos por ahora seas tú, pero ten en cuenta que las necesidades de un proyecto van evolucionando. Es por esta razón que uno de los valores más importantes del *software* es la tolerancia al cambio. Por ello, aunque estemos trabajando en un proyecto pequeño, debemos hacer el ejercicio de diferenciar las responsabilidades para conseguir que nuestro  *software* sea lo suficientemente flexible como para poder satisfacer las nuevas necesidades que puedan aparecer. 

## Aplicando el SRP

Volviendo al ejemplo, una forma de separar estas responsabilidades podría ser moviendo cada una de las funciones de la clase *UseCase* a otras, tal que así:

{title="Principio de responsabilidad única refactorizado", lang=javascript}
~~~~~~~
class UseCase{
  constructor(repo, notifier){
    this.repo = repo;
    this.notifier = notifier;
  }

  doSomethingWithTaxes(){
    console.log("Do something related with taxes ...")
  }

  saveChanges(){
    this.repo.update();
  }

  notify(){
    this.notifier.notify("Hi!")
  }
}


class Repository{
  add(){
    console.log("Adding in database");
  }

  update(){
    console.log("Updating in database...");
  }

  remove(){
    console.log("Deleting from database ...");
  }
    
  find(){
    console.log("Finding from database ...");
  }
}


class NotificationService{
  notify(message){
    console.log("Sending message ...");
    console.log(message);
  }
}


function start(){
  const repo = new Repository()
  const notifier = new NotificationService()
  const myUseCase = new UseCase(repo, notifier)

  myUseCase.doSomethingWithTaxes();
  myUseCase.saveChanges();
  myUseCase.notify();
}

start();
~~~~~~~

Puedes acceder al ejemplo interactivo [desde aquí](https://repl.it/@SoftwareCrafter/SOLID-SRP2).

La clase *UseCase* pasaría a representar un caso de uso con una responsabilidad más definida, ya que ahora el único actor relacionado con las clases es el encargado de la especificación de la operación *doSomethingWithTaxes()*. Para ello hemos extraído la implementación del resto de operaciones a las clases *Repository* y *NotificacionService*. 

La primera implementa un repositorio (como podrás comprobar es un repositorio *fake*) y se responsabiliza de todas las operaciones relacionadas con la persistencia. Por otro lado, la clase *NotificationService*, se encargaría de toda la lógica relacionada con las notificaciones al usuario. De esta manera ya tendríamos separadas las tres responsabilidades que habíamos detectado.

Ambas clases se inyectan vía constructor a la clase *UseCase*, pero, como puedes comprobar, se trata de implementaciones concretas, con lo cual el acoplamiento sigue siendo alto. Continuaremos profundizando en esto a lo largo de los siguientes capítulos, sobre todo en el de inversión de dependencias. 

## Detectar violaciones del SRP:

Saber si estamos respetando o no el principio de responsabilidad única puede ser, en ocasiones, un tanto ambiguo A continuación veremos un listado de situaciones que nos ayudarán a detectar violaciones del SRP: 

* **Nombre demasiado genérico**. Escoger un nombre excesivamente genérico suele derivar en un ***God Object***,  un objeto que hace demasiadas cosas. 

* **Los cambios suelen afectar a esta clase**. Cuando un elevado porcentaje de cambios suele afectar a la misma clase, puede ser síntoma de que dicha clase está demasiado acoplada o tiene demasiadas responsabilidades.

* **La clase involucra múltiples capas de la arquitectura.** Si, como vimos en el caso del ejemplo, nuestra clase hace cosas como acceder a la capa de persistencia o notificar al usuario, además de implementar la lógica de negocio, está violando claramente el SRP.

* **Número alto de *imports***. Aunque esto por sí mismo no implica nada, podría ser un síntoma de violación.

* **Cantidad elevada de métodos públicos.** Cuando una clase tiene una API con un número alto de métodos públicos, suele ser síntoma de que tiene demasiadas responsabilidades.

* **Excesivo número de líneas de código**. Si nuestra clase solo tiene una única responsabilidad, su número de líneas no debería, en principio, ser muy elevado. 

