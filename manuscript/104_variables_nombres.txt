# Variables, nombres y ámbito

**"Nuestro código tiene que ser simple y directo, debería leerse con la misma facilidad que un texto bien escrito".** -- [Grady Booch](https://es.wikipedia.org/wiki/Grady_Booch)

Nuestro código debería poder leerse con la misma facilidad con la que leemos un texto bien escrito, es por ello que escoger buenos nombres, hacer un uso correcto de la declaración de las variables y entender el concepto de ámbito es fundamental en JavaScript. 

{width=100%,float=center}
![Viñeta de Commit Strip sobre el nombrado de variables.](images/problem-naming.jpg)

Los nombres de variables, funciones o métodos y clases deben seleccionarse con cuidado para que den expresividad y significado a nuestro código. En este capítulo, además de profundizar en algunos detalles importantes relacionados con las variables y su ámbito, veremos algunas pautas y ejemplos para tratar de mejorar a la hora de escoger buenos nombres.

## Uso correcto de *var, let* y *const*

En JavaScript clásico, antes de ES6, únicamente teníamos una forma de declarar las variables y era a través del uso de la palabra *var*. A partir de ES6 se introducen *let* y *const*, con lo que pasamos a tener tres palabras reservadas para la declaración de variables.

Lo ideal sería tratar de evitar a toda costa el uso de *var*, ya que no permite definir variables con un ámbito de bloque, lo cual puede derivar en comportamientos inesperados y poco intuitivos. Esto no ocurre con las variables definidas con *let* y *const*, que sí permiten definir este tipo de ámbito, como veremos al final de este capítulo.

La diferencia entre *let* y *const* radica en que a esta última no se le puede reasignar su valor, aunque sí modificarlo. Es decir, se puede modificar (mutar) en el caso de un objeto, pero no si se trata de un tipo primitivo. Por este motivo, usar *const* en variables en las que no tengamos pensado cambiar su valor puede ayudarnos a mejorar la intencionalidad de nuestro código.

{title="Ejemplo de uso de var", lang=javascript}
~~~~~~~
var variable = 5;
{
  console.log('inside', variable); // 5
  var variable = 10;
}

console.log('outside', variable); // 10
variable = variable * 2;
console.log('changed', variable); // 20
~~~~~~~

Puedes acceder al ejemplo interactivo [desde aquí](https://repl.it/@SoftwareCrafter/CLEAN-CODE-var)

{title="Ejemplo de uso de let", lang=javascript}
~~~~~~~
let variable = 5;

{
  console.log('inside', variable); // error
  let variable = 10;
}

console.log('outside', variable); // 5
variable = variable * 2;
console.log('changed', variable); // 10
~~~~~~~

Puedes acceder al ejemplo interactivo [desde aquí](https://repl.it/@SoftwareCrafter/CLEAN-CODE-let)

{title="Ejemplo de uso de const", lang=javascript}
~~~~~~~
const variable = 5;
variable = variable*2; // error
console.log('changed', variable); // doesn't get here
~~~~~~~

Puedes acceder al ejemplo interactivo [desde aquí](https://repl.it/@SoftwareCrafter/CLEAN-CODE-const)

## Nombres pronunciables y expresivos

Los nombres de las variables, imprescindiblemente en inglés, deben ser pronunciables. Esto quiere decir que no deben ser abreviaturas ni llevar guión bajo o medio, priorizando el estilo CamelCase. Por otro lado, debemos intentar no ahorrarnos caracteres en los nombres, la idea es que sean lo más expresivos posible.

{title="Nombres pronunciables y expresivos", lang=javascript}
~~~~~~~
//bad
const yyyymmdstr = moment().format('YYYY/MM/DD');

//better
const currentDate = moment().format('YYYY/MM/DD');
~~~~~~~

## Ausencia de información técnica en los nombres

Si estamos construyendo un *software* de tipo vertical (orientado a negocio), debemos intentar que los nombres no contengan información técnica en ellos, es decir, evitar incluir información relacionada con la tecnología, como el tipo de dato o [la notación húngara](https://es.wikipedia.org/wiki/Notaci%C3%B3n_h%C3%BAngara), el tipo de clase, etc. Esto sí se admite en desarrollo de *software* horizontal o librerías de propósito general.

{title="Evitar que los nombres contengan información técnica", lang=javascript}
~~~~~~~
//bad
class AbstractUser(){...}

//better
class User(){...}
~~~~~~~

## Establecer un lenguaje ubicuo
El término “lenguaje ubicuo” lo introdujo Eric Evans en su famoso libro sobre DDD, *Implementing Domain-Driven Design*, también conocido como “el libro rojo del DDD”. Aunque el DDD queda fuera del ámbito de este libro, creo que hacer uso del lenguaje ubicuo es tremendamente importante a la hora de obtener un léxico coherente. 

El lenguaje ubicuo es un proceso en el cual se trata de establecer un lenguaje común entre programadores y *stakeholders* (expertos de dominio), basado en las definiciones y terminología que se emplean en el negocio.

Una buena forma de comenzar con este el proceso podría ser crear un glosario de términos. Esto nos permitirá, por un lado, mejorar la comunicación con los expertos del negocio, y por otro, ayudarnos a escoger nombres más precisos para mantener una nomenclatura homogénea en toda la aplicación.

Por otro lado, debemos usar el mismo vocabulario para hacer referencia al mismo concepto, no debemos usar en algunos lados *User*, en otro *Client* y en otro *Customer*, a no ser que representen claramente conceptos diferentes.

{title="Léxico coherente", lang=javascript}
~~~~~~~
//bad
getUserInfo();
getClientData();
getCustomerRecord();

//better
getUser()
~~~~~~~

## Nombres según el tipo de dato

### Arrays

Los *arrays* son una lista iterable de elementos, generalmente del mismo tipo. Es por ello que pluralizar el nombre de la variable puede ser una buena idea:

{title=" Arrays", lang=javascript}
~~~~~~~
//bad
const fruit = ['manzana', 'platano', 'fresa'];
// regular
const fruitList = ['manzana', 'platano', 'fresa'];
// good
const fruits = ['manzana', 'platano', 'fresa'];
// better
const fruitNames = ['manzana', 'platano', 'fresa'];
~~~~~~~

### Booleanos

Los *booleanos* solo pueden tener 2 valores: verdadero o falso. Dado esto, el uso de prefijos como *is*, *has* y *can* ayudará inferir el tipo de variable, mejorando así la legibilidad de nuestro código.

{title="Booleanos", lang=javascript}
~~~~~~~
//bad
const open = true;
const write = true;
const fruit = true;

// good
const isOpen = true;
const canWrite = true;
const hasFruit = true;
~~~~~~~

### Números
Para los números es interesante escoger palabras que describan números, como *min*, *max* o *total*:

{title="Números", lang=javascript}
~~~~~~~
//bad
const fruits = 3;

//better
const maxFruits = 5;
const minFruits = 1;
const totalFruits = 3;
~~~~~~~

### Funciones
Los nombres de las funciones deben representar acciones, por lo que deben construirse usando el verbo que representa la acción seguido de un sustantivo. Estos deben de ser descriptivos y, a su vez, concisos. Esto quiere decir que el nombre de la función debe expresar lo que hace, pero también debe de abstraerse de la implementación de la función.

{title="Funciones", lang=javascript}
~~~~~~~
//bad
createUserIfNotExists()
updateUserIfNotEmpty()
sendEmailIfFieldsValid()

//better
createUser(...)
updateUser(...)
sendEmail()
~~~~~~~

En el caso de las funciones de acceso, modificación o predicado, el nombre debe ser el prefijo *get*, *set* e *is*, respectivamente.

{title="Funciones de acceso, modificación o predicado", lang=javascript}
~~~~~~~
getUser()
setUser(...)
isValidUser()
~~~~~~~

####*Get* y *set*

En el caso de los *getters* y *setters*, sería interesante hacer uso de las palabras clave *get* y *set* cuando estamos accediendo a propiedades de objetos. Estas se introdujeron en ES6 y nos permiten definir métodos accesores:

{title="Get y set", lang=javascript}
~~~~~~~
class Person {
  constructor(name) {
    this._name = name;
  }

  get name() {
    return this._name;
  }

  set name(newName) {
    this._name = newName;
  }
}

let person = new Person(‘Miguel’);
console.log(person.name); // Outputs ‘Miguel’
~~~~~~~

### Clases

Las clases y los objetos deben tener nombres formados por un sustantivo o frases de sustantivo como *User, UserProfile, Account* o *AdressParser*. Debemos evitar nombres genéricos como *Manager, Processor, Data* o *Info*.

Hay que ser cuidadosos a la hora de escoger estos nombres, ya que son el paso previo a la hora de definir la responsabilidad de la clase. Si escogemos nombres demasiado genéricos tendemos a crear clases con múltiples responsabilidades.

## Ámbito o *scope* de las variables

Además de escribir nombres adecuados para las variables, es fundamental entender cómo funciona su *scope* en JavaScript. El *scope*, que se traduce como “ámbito” o “alcance” en español, hace referencia a la visibilidad y a la vida útil de una variable. El ámbito, en esencia, determina en qué partes de nuestro programa tenemos acceso a una cierta variable. 

En JavaScript existen principalmente tres tipos de ámbitos: el ámbito global, el ámbito local o de función y el ámbito de bloque.

### Ámbito global

Cualquier variable que no esté dentro de un bloque de una función, estará dentro del ámbito global. Dichas variables serán accesibles desde cualquier parte de la aplicación:

{title="Ámbito global", lang=javascript}
~~~~~~~
let greeting = ‘hello world!’;

function greet(){
	console.log(greeting);
}

greet(); //”Hello world”;
~~~~~~~

### Ámbito de bloque

Los bloques en Javascript se delimitan mediante llaves, una de apertura ‘{‘, y otra de cierre ‘}’. Como comentamos en el apartado de “Uso correcto de *var*, *let* y *const*”, para definir variables con alcance de bloque debemos hacer uso de *let* o *const*:

{title="Ámbito de bloque", lang=javascript}
~~~~~~~
{
	let greeting = “Hello world!”;
	var lang = “English”;
	console.log(greeting); //Hello world!
}

console.log(lang);//”English”
console.log(greeting);//// Uncaught ReferenceError: greeting is not defined		
~~~~~~~

En este ejemplo queda patente que las variables definidas con *var* se pueden emplear fuera del bloque, ya que este tipo de variables no quedan encapsuladas dentro de los bloques. Por este motivo, y de acuerdo con lo mencionado anteriormente, debemos evitar su uso para no encontrarnos con comportamientos inesperados. 

### Ámbito estático vs. dinámico

El ámbito de las variables en JavaScript tiene un comportamiento de naturaleza estática. Esto quiere decir que se determina en tiempo de compilación en lugar de en tiempo de ejecución. Esto también se suele denominar **ámbito léxico (*lexical scope*).** Veamos un ejemplo:

{title="Ámbito estático vs. dinámico", lang=javascript}
~~~~~~~
const number = 10;
function printNumber() {
  console.log(number); 
}

function app() {
  const number = 5;
  printNumber();
}

app(); //10		
~~~~~~~

En el ejemplo, *console.log (number)* siempre imprimirá el número 10 sin importar desde dónde se llame la función *printNumber()*. Si JavaScript fuera un lenguaje con el ámbito dinámico, *console.log(number)* imprimiría un valor diferente dependiendo de dónde se ejecutará la función *printNumber()*.

### *Hoisting*

En JavaScript las declaraciones de las variables y funciones se asignan en memoria en tiempo de compilación; a nivel práctico es como si el intérprete moviera dichas declaraciones al principio de su ámbito. Este comportamiento es conocido como **hoisting**.
Gracias al *hoisting* podríamos ejecutar una función antes de su declaración:

{title="Hoisting", lang=javascript}
~~~~~~~
greet(); //”Hello world”;
function greet(){
    let greeting = ‘Hello world!’;
    console.log(greeting);
}
~~~~~~~

Al asignar la declaración en memoria es como si “subiera” la función al principio de su ámbito:

{title="Hoisting", lang=javascript}
~~~~~~~
function greet(){
    let greeting = ‘Hello world!’;
    console.log(greeting);
}
greet(); //”Hello world”;
~~~~~~~

En el caso de las variables, el *hoisting* puede generar comportamientos inesperados, ya que como hemos dicho solo aplica a la declaración y no a su asignación:

{title="Hoisting", lang=javascript}
~~~~~~~
var greet = "Hi";
(function () {
    console.log(greet);// "undefined"
    var greet = "Hello";
    console.log(greet); //”Hello”
})();
~~~~~~~

En el primer *console.log* del ejemplo, lo esperado es que escriba “Hi”, pero como hemos comentado, el intérprete “eleva” la declaración de la variable a la parte superior de su *scope*. Por lo tanto, el comportamiento del ejemplo anterior sería equivalente a escribir el siguiente código:

{title="Hoisting", lang=javascript}
~~~~~~~
var greet = "Hi";
(function () {
    var greet;
    console.log(greet);// "undefined"
    greet = "Hello";
    console.log(greet); //”Hello”
})();
~~~~~~~

He usado este ejemplo porque creo que es muy ilustrativo para explicar el concepto de **hoisting**, pero volver a declarar una variable con el mismo nombre y además usar *var* para definirlas es muy mala idea.

