# Sobre el autor

Mi nombre es Miguel A. Gómez, soy de Tenerife y estudié ingeniería en Radioelectrónica e Ingeniería en Informática. Me considero un artesano de software (**Software Craftsman**), sin los dogmatismos propios de la comunidad y muy interesado en el desarrollo de software con [Haskell](https://www.haskell.org/).

Actualmente trabajo como **Senior Software Engineer** en una *startup* estadounidense dedicada al desarrollo de soluciones software para el sector de la abogacía, en la cual he participado como desarrollador principal en diferentes proyectos. 

Entre los puestos más importantes destacan **desarrollador móvil multiplataforma con Xamarin y C#** y  el de desarrollador **fullStack**, el puesto que desempeño actualmente. En este último, aplico un estilo de programación híbrido entre orientación a objetos y [programacion funcional reactiva (FRP)](https://en.wikipedia.org/wiki/Functional_reactive_programming), tanto para el **frontend** con [Typescript](https://softwarecrafters.io/typescript/typescript-javascript-introduccion), [RxJS](https://softwarecrafters.io/javascript/introduccion-programacion-reactiva-rxjs) y [ReactJS](https://softwarecrafters.io/react/tutorial-react-js-introduccion), como para el **backend** con **Typescript, NodeJS, RxJS y MongoDB**, además de gestionar los procesos [DevOps](https://es.wikipedia.org/wiki/DevOps) con **Docker** y **Azure**.

Por otro lado, soy cofundador de la start-up [Omnirooms.com](https://www.omnirooms.com), un proyecto con el cual pretendemos eliminar las barreras con las que se encuentran las personas con movilidad reducida a la hora de reservar sus vacaciones. Además, soy fundador de [SoftwareCrafters.io](https://softwarecrafters.io/), una comunidad sobre artesanía del software, DevOps y tecnologías software con aspiraciones a plataforma de formación y consultoría.

